package com.company.team.controller.admin;


import com.company.team.data.entity.OrderEntity;
import com.company.team.data.response.base.MyResponse;
import com.company.team.service.implement.OrderService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/api")
public class OrderManagerController {

    private final OrderService orderService;

    public OrderManagerController(OrderService orderService) {
        this.orderService = orderService;
    }


    @GetMapping("/admin/auth/v1/order-history")
    public ResponseEntity<?> orderHistory() {
        MyResponse response = MyResponse
                .builder()
                .buildCode(200)
                .buildMessage("not implement")
                .get();

        return ResponseEntity.ok(response);
    }

    @GetMapping("/admin/auth/v1/order-history/{orderId}")
    public ResponseEntity<?> getOrderDetail(@PathVariable("orderId") int orderId) {

        OrderEntity order = orderService.findOne(orderId);

        MyResponse response = MyResponse
                .builder()
                .buildCode(200)
                .buildMessage("Success")
                .buildData(order)
                .get();

        return ResponseEntity.ok(response);
    }
}
