package com.company.team.data.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class OrderRequest {

    String customerName;
    String phoneNumber;
    String address;

    String zipcode;
    String city;
    String state;
    String country;
    double shippingPrice;

    String email;

}
