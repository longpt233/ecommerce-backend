package com.company.team.data.mapper;

import com.company.team.data.entity.CategoryEntity;
import com.company.team.data.entity.ProductEntity;
import com.company.team.data.request.ProductRequest;
import com.company.team.data.response.dto.ProductDto;
import com.company.team.exception.custom.NotFoundException;
import com.company.team.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

import static com.company.team.utils.json.JsonSerializable.*;

/**
 * map from entity -> model
 */

@Service
public class ProductMapper {

    @Autowired
    private CategoryRepository categoryRepository;

    public  ProductDto productEntityToDto(ProductEntity product) {
        ProductDto productDto = new ProductDto();

        productDto.setProductId(product.getId());
        productDto.setName(product.getName());
        productDto.setDescription(product.getDescription());
        productDto.setPrice(product.getPrice());
        productDto.setBrand(product.getBrand());
        productDto.setImage(deserialize(product.getImageUrl(), Set.class));
        productDto.setCategoryId(product.getCategory().getCategoryId());
        productDto.setStarCount(product.getRating());
        productDto.setQuantity(product.getAvailable());
        productDto.setDiscountPercent(product.getDiscountPercent());

        return productDto;
    }

    public  ProductEntity productReqToProductEntityUpdate(ProductRequest productRequest, long id) {
        ProductEntity product = productReqToProductEntity(productRequest);
        product.setId(id);
        return product;
    }

    public  ProductEntity productReqToProductEntity(ProductRequest productRequest) {
        ProductEntity product = new ProductEntity();

        long idCate = productRequest.getCategoryId();
        Optional<CategoryEntity> queryCate = categoryRepository.findById(idCate);
        if (!queryCate.isPresent()) throw new NotFoundException("id cate does not exist");
        product.setCategory(queryCate.get());

        product.setName(productRequest.getName());
        product.setAvailable(productRequest.getQuantity());
        product.setBrand(productRequest.getBrand());
        product.setPrice(productRequest.getPrice());
        product.setImageUrl(serialize(productRequest.getImage()));
        product.setDescription(productRequest.getDescription());
        product.setRating(productRequest.getStarCount());
        product.setDiscountPercent(productRequest.getDiscountPercent());


        return product;
    }
}
