package com.company.team.data.mapper;

import com.company.team.data.entity.CategoryEntity;
import com.company.team.data.entity.ProductEntity;
import com.company.team.data.request.CategoryRequest;
import com.company.team.data.response.dto.CategoryDto;
import com.company.team.data.response.dto.CategoryWithProdDto;
import com.company.team.data.response.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service
public class CategoryMapper {

    @Autowired
    private ProductMapper productMapper;
    public  CategoryDto cateEntityToDto(CategoryEntity category) {

        CategoryDto categoryViewModel = new CategoryDto();

        categoryViewModel.setCategoryId(category.getCategoryId());
        categoryViewModel.setName(category.getName());
        categoryViewModel.setDescription(category.getDescription());
        categoryViewModel.setCreatedDate(new Date());

        return categoryViewModel;
    }

    public  CategoryWithProdDto cateEntityToCateProdDto(CategoryEntity category) {

        CategoryWithProdDto categoryViewModel = new CategoryWithProdDto();

        categoryViewModel.setTitle(category.getName());

        Set<ProductDto> productDtos = category
                .getProductEntities()
                .stream()
                .map(e-> productMapper.productEntityToDto(e))
                .collect(Collectors.toSet());

        categoryViewModel.setListProduct(productDtos);

        return categoryViewModel;
    }

    public  CategoryEntity toCategoryEntity(CategoryRequest category, long categoryId) {

        CategoryEntity categoryEntity = toCategoryEntity(category);
        categoryEntity.setCategoryId(categoryId);

        return categoryEntity;
    }

    public  CategoryEntity toCategoryEntity(CategoryRequest category) {

        CategoryEntity categoryEntity = new CategoryEntity();

        categoryEntity.setName(category.getName());
        categoryEntity.setDescription(category.getDescription());

        return categoryEntity;
    }

}
