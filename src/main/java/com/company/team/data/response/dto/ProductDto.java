package com.company.team.data.response.dto;


import lombok.Data;

import java.util.List;
import java.util.Set;

/**
 * final model view return to client
 */

@Data
public class ProductDto {

    long productId;
    long categoryId;
    String name;
    String description;
    Double price;
    int discountPercent;
    int quantity;
    Set<String> image;
    String brand;
    int starCount;

}
