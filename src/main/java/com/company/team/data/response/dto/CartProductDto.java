package com.company.team.data.response.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
public class CartProductDto {

    ProductDto productInfo;
    String size;
    String color;
    int amount;

}
